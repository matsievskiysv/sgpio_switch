alias mstep = monitor step
alias mhalt = monitor reset halt
alias mrerun = monitor reset run
alias mcontinue = monitor resume
alias mstop = monitor halt
alias mread = monitor mdw
alias mload_image = monitor load_image
alias mreg = monitor reg
alias mflash_info = monitor flash banks

define mflash_load
  monitor flash write_image erase unlock $arg0
  monitor flash verify_image $arg0
  monitor reset halt
end

alias phex = p/x
alias pbin = p/t
alias xhex = x/x
alias xbin = x/t
