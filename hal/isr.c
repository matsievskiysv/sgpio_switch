extern void _stack_start(void);

__attribute__((weak, interrupt)) void
reset_handler(void)
{
	for (;;) {}
}

__attribute__((weak, interrupt)) void
default_handler(void)
{
	for (;;) {}
}

__attribute__((weak, interrupt)) void
hard_fault_handler(void)
{
	for (;;) {}
}

void nmi_handler(void) __attribute__((weak, interrupt, alias("hard_fault_handler")));
void svc_handler(void) __attribute__((weak, interrupt, alias("default_handler")));
void pendsv_handler(void) __attribute__((weak, interrupt, alias("default_handler")));
void systick_handler(void) __attribute__((weak, interrupt, alias("default_handler")));
void wwdg_irqhandler(void) __attribute__((weak, interrupt, alias("default_handler")));
void rtc_irqhandler(void) __attribute__((weak, interrupt, alias("default_handler")));
void flash_irqhandler(void) __attribute__((weak, interrupt, alias("default_handler")));
void rcc_irqhandler(void) __attribute__((weak, interrupt, alias("default_handler")));
void exti0_1_irqhandler(void) __attribute__((weak, interrupt, alias("default_handler")));
void exti2_3_irqhandler(void) __attribute__((weak, interrupt, alias("default_handler")));
void exti4_15_irqhandler(void) __attribute__((weak, interrupt, alias("default_handler")));
void dma1_channel1_irqhandler(void) __attribute__((weak, interrupt, alias("default_handler")));
void dma1_channel2_3_irqhandler(void) __attribute__((weak, interrupt, alias("default_handler")));
void dma1_channel4_5_irqhandler(void) __attribute__((weak, interrupt, alias("default_handler")));
void adc_irqhandler(void) __attribute__((weak, interrupt, alias("default_handler")));
void tim1_brk_irqhandler(void) __attribute__((weak, interrupt, alias("default_handler")));
void tim1_cc_irqhandler(void) __attribute__((weak, interrupt, alias("default_handler")));
void tim3_irqhandler(void) __attribute__((weak, interrupt, alias("default_handler")));
void tim6_irqhandler(void) __attribute__((weak, interrupt, alias("default_handler")));
void tim14_irqhandler(void) __attribute__((weak, interrupt, alias("default_handler")));
void tim15_irqhandler(void) __attribute__((weak, interrupt, alias("default_handler")));
void tim16_irqhandler(void) __attribute__((weak, interrupt, alias("default_handler")));
void tim17_irqhandler(void) __attribute__((weak, interrupt, alias("default_handler")));
void i2c1_irqhandler(void) __attribute__((weak, interrupt, alias("default_handler")));
void i2c2_irqhandler(void) __attribute__((weak, interrupt, alias("default_handler")));
void spi1_irqhandler(void) __attribute__((weak, interrupt, alias("default_handler")));
void spi2_irqhandler(void) __attribute__((weak, interrupt, alias("default_handler")));
void usart1_irqhandler(void) __attribute__((weak, interrupt, alias("default_handler")));
void usart2_irqhandler(void) __attribute__((weak, interrupt, alias("default_handler")));
void usart3_4_5_6_irqhandler(void) __attribute__((weak, interrupt, alias("default_handler")));
void usb_irqhandler(void) __attribute__((weak, interrupt, alias("default_handler")));
void usbwakeup_irqhandler(void) __attribute__((weak, interrupt, alias("default_handler")));

#if defined(stm32f030f4)
#include "stm32f030f4/isr.c"
#else
#error Board not selected
#endif
