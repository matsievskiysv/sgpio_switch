#include <usart.h>
#include <util.h>

#ifdef __FPU_PRESENT
#include <math.h>
#endif

void
usart_baudrate(size_t port, int freq, int baud)
{
#ifdef __FPU_PRESENT
	float  fraction_real = freq / 16.0 / baud;
	size_t mantissa	     = floorf(fraction_real);
	size_t fraction	     = floorf((fraction_real - mantissa) * (USART_BAUD_FRACTION_MASK + 1));
#else
	size_t mantissa	     = freq / 16 / baud;
	size_t fraction_part = freq - mantissa * 16 * baud;
	size_t fraction	     = fraction_part / 16 * (USART_BAUD_FRACTION_MASK + 1) / baud;
#endif
	REG_T	    reg = GET_REG(port, USART_BRR);
	usart_bbr_t bbr = {.bits = {.mantissa = mantissa, .fraction = fraction}};
	REG_WRITE(reg, bbr.val);
}

void
usart_configure(size_t port, bool nine_bit, bool parity, bool even_parity, bool send_break, size_t stop_bits)
{
	REG_T	    cr1_reg = GET_REG(port, USART_CR1);
	usart_cr1_t cr1	    = {.bits = {.m = nine_bit, .pce = parity, .ps = even_parity, .sbk = send_break}};
	REG_SET(cr1_reg, cr1.val);
	REG_T	    cr2_reg = GET_REG(port, USART_CR2);
	usart_cr2_t cr2	    = {.bits = {.stop = stop_bits}};
	REG_SET(cr2_reg, cr2.val);
}

uint8_t
usart_read(size_t port)
{
	REG_T	   dr	= GET_REG(port, USART_DR);
	usart_dr_t data = {.val = REG_VAL(dr)};
	return data.bits.dr;
}

void
usart_write(size_t port, uint8_t data)
{
	REG_T dr = GET_REG(port, USART_DR);
	REG_WRITE(dr, data);
}

usart_sr_t
usart_status(size_t port)
{
	REG_T	   dr	  = GET_REG(port, USART_SR);
	usart_sr_t status = {.val = REG_VAL(dr)};
	return status;
}

void
usart_status_clear(size_t port, usart_sr_t status)
{
	REG_T dr = GET_REG(port, USART_SR);
	REG_CLEAR(dr, status.val);
}

void
usart_enable(size_t port)
{
	REG_T	    cr1_reg = GET_REG(port, USART_CR1);
	usart_cr1_t cr1	    = {.bits = {.re = true, .te = true, .ue = true}};
	REG_SET(cr1_reg, cr1.val);
}

void
usart_disable(size_t port)
{
	REG_T	    cr1_reg = GET_REG(port, USART_CR1);
	usart_cr1_t cr1	    = {.bits = {.re = true, .te = true, .ue = true}};
	REG_CLEAR(cr1_reg, cr1.val);
}

void
usart_configure_interrupt(size_t port, bool parity_err_int, bool tx_buf_empty_int, bool tx_complete_int,
			  bool rx_buf_not_empty_int, bool idle_int, bool err_int)
{
	REG_T	    cr1_reg = GET_REG(port, USART_CR1);
	usart_cr1_t cr1	    = {.bits = {.peie	= parity_err_int,
					.txeie	= tx_buf_empty_int,
					.tcie	= tx_complete_int,
					.rxneie = rx_buf_not_empty_int,
					.idleie = idle_int}};
	REG_SET(cr1_reg, cr1.val);
	REG_T	    cr3_reg = GET_REG(port, USART_CR3);
	usart_cr3_t cr3	    = {.bits = {.eie = err_int}};
	REG_SET(cr3_reg, cr3.val);
}

void
usart_dma(size_t port, bool dma_tx, bool dma_rx)
{
	REG_T cr3_reg = GET_REG(port, USART_CR3);
	{
		usart_cr3_t cr3 = {.bits = {.dmat = true, .dmar = true}};
		REG_CLEAR(cr3_reg, cr3.val);
	}
	usart_cr3_t cr3 = {.bits = {.dmat = dma_tx, .dmar = dma_rx}};
	REG_SET(cr3_reg, cr3.val);
}
