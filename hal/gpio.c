#include <gpio.h>
#include <util.h>

void
gpio_configure_input(size_t bank, uint8_t pin_number, uint8_t mode, uint8_t pull)
{
	size_t mask = 0xf;
	mask <<= pin_number * 2;
	{
		REG_T reg = GET_REG(bank, GPIO_MODER);
		mode <<= pin_number * 2;
		// write all zeros before setting value
		REG_CLEAR(reg, mask);
		REG_SET(reg, mode);
	}
	{
		REG_T reg = GET_REG(bank, GPIO_PUPDR);
		pull <<= pin_number * 2;
		// write all zeros before setting value
		REG_CLEAR(reg, mask);
		REG_SET(reg, pull);
	}
}

void
gpio_configure_output(size_t bank, uint8_t pin_number, uint8_t mode, uint8_t pull, uint8_t speed)
{
	size_t mask = 0xf;
	mask <<= pin_number * 2;
	{
		REG_T  reg = GET_REG(bank, GPIO_MODER);
		size_t val = GPIO_MODE_OUT << (pin_number * 2);
		// write all zeros before setting value
		REG_CLEAR(reg, mask);
		REG_SET(reg, val);
	}
	{
		REG_T reg = GET_REG(bank, GPIO_OTYPER);
		if (mode == GPIO_OUT_PUSHPULL)
			REG_SET(reg, BIT(pin_number));
		else
			REG_CLEAR(reg, BIT(pin_number));
	}
	{
		REG_T reg = GET_REG(bank, GPIO_PUPDR);
		pull <<= pin_number * 2;
		// write all zeros before setting value
		REG_CLEAR(reg, mask);
		REG_SET(reg, pull);
	}
	{
		REG_T reg = GET_REG(bank, GPIO_OSPEEDR);
		speed <<= pin_number * 2;
		// write all zeros before setting value
		REG_CLEAR(reg, mask);
		REG_SET(reg, speed);
	}
}

void
gpio_configure_alt(size_t bank, uint8_t pin_number, uint8_t pull)
{
	size_t mask = 0xf;
	mask <<= pin_number * 2;
	{
		REG_T  reg = GET_REG(bank, GPIO_MODER);
		size_t val = GPIO_MODE_ALT << (pin_number * 2);
		// write all zeros before setting value
		REG_CLEAR(reg, mask);
		REG_SET(reg, val);
	}
	{
		REG_T reg = GET_REG(bank, GPIO_PUPDR);
		pull <<= pin_number * 2;
		// write all zeros before setting value
		REG_CLEAR(reg, mask);
		REG_SET(reg, pull);
	}
}

size_t
gpio_read_pins(size_t bank, size_t pin_mask)
{
	REG_T dr = GET_REG(bank, GPIO_IDR);
	return REG_VAL(dr) & pin_mask;
}

bool
gpio_read_pin(size_t bank, uint8_t pin_number)
{
	return !!gpio_read_pins(bank, BIT(pin_number));
}

void
gpio_write_pins(size_t bank, size_t pin_mask, bool value)
{
	REG_T reg = GET_REG(bank, GPIO_BSRR);
	REG_SET(reg, value ? pin_mask : pin_mask << 16);
}

void
gpio_write_pin(size_t bank, uint8_t pin_number, bool value)
{
	REG_T reg = GET_REG(bank, GPIO_BSRR);
	REG_SET(reg, value ? BIT(pin_number) : BIT(pin_number + 16));
}
