#pragma once

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#define USART1 0x4001##3800##UL
#define USART2 0x4000##4400##UL
#define USART3 0x4000##4800##UL
#define UART4  0x4000##4C00##UL
#define UART5  0x4000##5000##UL

#define USART_SR   0x00
#define USART_DR   0x04
#define USART_BRR  0x08
#define USART_CR1  0x0C
#define USART_CR2  0x10
#define USART_CR3  0x14
#define USART_GTPR 0x18

typedef union {
	struct {
		bool pe	  : 1;
		bool fe	  : 1;
		bool ne	  : 1;
		bool ore  : 1;
		bool idle : 1;
		bool rxne : 1;
		bool tc	  : 1;
		bool txe  : 1;
		bool lbd  : 1;
		bool cts  : 1;
	} bits;
	size_t val;
} usart_sr_t;

typedef union {
	struct {
		uint8_t dr : 8;
	} bits;
	size_t val;
} usart_dr_t;

typedef union {
	struct {
		uint8_t fraction : 4;
		size_t	mantissa : 12;
	} bits;
	size_t val;
} usart_bbr_t;

#define USART_BAUD_FRACTION_MASK 0b1111

typedef union {
	struct {
		bool sbk    : 1;
		bool rwu    : 1;
		bool re	    : 1;
		bool te	    : 1;
		bool idleie : 1;
		bool rxneie : 1;
		bool tcie   : 1;
		bool txeie  : 1;
		bool peie   : 1;
		bool ps	    : 1;
		bool pce    : 1;
		bool wake   : 1;
		bool m	    : 1;
		bool ue	    : 1;
	} bits;
	size_t val;
} usart_cr1_t;

#define USART_8BITS	     0
#define USART_9BITS	     1
#define USART_WAKE_IDLE	     0
#define USART_WAKE_ADDRESS   1
#define USART_PARITY_DISABLE 0
#define USART_PARITY_ENABLE  1
#define USART_PARITY_ODD     0
#define USART_PARITY_EVEN    1
#define USART_RX_ACTIVE	     0
#define USART_RX_MUTE	     1
#define USART_NO_BREAK	     0
#define USART_BREAK	     1

typedef union {
	struct {
		uint8_t add   : 4;
		bool	      : 1;
		bool lbdl     : 1;
		bool lbdie    : 1;
		bool	      : 1;
		bool	lbcl  : 1;
		bool	cpha  : 1;
		bool	cpol  : 1;
		bool	clken : 1;
		uint8_t stop  : 2;
		bool	linen : 1;
	} bits;
	size_t val;
} usart_cr2_t;

#define USART_STOP_1   0b00
#define USART_STOP_0_5 0b01
#define USART_STOP_2   0b10
#define USART_STOP_1_5 0b11

#define UART_STOP_1 USART_STOP_1
#define UART_STOP_2 USART_STOP_2

#define USART_CLK_POL_LOW 0
#define USART_CLK_POL_HI  1
#define USART_CLK_PH_1	  0
#define USART_CLK_PH_2	  1
#define USART_BR_DET_10	  0
#define USART_BR_DET_11	  1

typedef union {
	struct {
		bool eie   : 1;
		bool iren  : 1;
		bool irlp  : 1;
		bool hdsel : 1;
		bool nack  : 1;
		bool scen  : 1;
		bool dmar  : 1;
		bool dmat  : 1;
		bool rtse  : 1;
		bool ctse  : 1;
		bool ctsie : 1;
	} bits;
	size_t val;
} usart_cr3_t;

typedef union {
	struct {
		uint8_t psc : 8;
		uint8_t gt  : 8;
	} bits;
	size_t val;
} usart_gtpr_t;
