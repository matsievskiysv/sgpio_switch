#pragma once

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#define GPIO_A 0x4800##0000##UL
#define GPIO_B 0x4800##0400##UL
#define GPIO_C 0x4800##0800##UL
#define GPIO_D 0x4800##0C00##UL
#define GPIO_F 0x4800##1400##UL

#define GPIO_MODER   0x00
#define GPIO_OTYPER  0x04
#define GPIO_OSPEEDR 0x08
#define GPIO_PUPDR   0x0C
#define GPIO_IDR     0x10
#define GPIO_ODR     0x14
#define GPIO_BSRR    0x18
#define GPIO_LCKR    0x1C
#define GPIO_AFRL    0x20
#define GPIO_AFRH    0x24
#define GPIO_BRR     0x28

typedef union {
	struct {
		uint8_t mode0  : 2;
		uint8_t mode1  : 2;
		uint8_t mode2  : 2;
		uint8_t mode3  : 2;
		uint8_t mode4  : 2;
		uint8_t mode5  : 2;
		uint8_t mode6  : 2;
		uint8_t mode7  : 2;
		uint8_t mode8  : 2;
		uint8_t mode9  : 2;
		uint8_t mode10 : 2;
		uint8_t mode11 : 2;
		uint8_t mode12 : 2;
		uint8_t mode13 : 2;
		uint8_t mode14 : 2;
		uint8_t mode15 : 2;
	} bits;
	size_t val;
} gpio_moder_t;

#define GPIO_MODE_INPUT	 0b00
#define GPIO_MODE_OUT	 0b01
#define GPIO_MODE_ALT	 0b10
#define GPIO_MODE_ANALOG 0b11

typedef union {
	struct {
		bool ot0  : 1;
		bool ot1  : 1;
		bool ot2  : 1;
		bool ot3  : 1;
		bool ot4  : 1;
		bool ot5  : 1;
		bool ot6  : 1;
		bool ot7  : 1;
		bool ot8  : 1;
		bool ot9  : 1;
		bool ot10 : 1;
		bool ot11 : 1;
		bool ot12 : 1;
		bool ot13 : 1;
		bool ot14 : 1;
		bool ot15 : 1;
	} bits;
	size_t val;
} gpio_crh_t;

#define GPIO_OUT_PUSHPULL  0b0
#define GPIO_OUT_OPENDRAIN 0b1

typedef union {
	struct {
		uint8_t ospeedr0  : 2;
		uint8_t ospeedr1  : 2;
		uint8_t ospeedr2  : 2;
		uint8_t ospeedr3  : 2;
		uint8_t ospeedr4  : 2;
		uint8_t ospeedr5  : 2;
		uint8_t ospeedr6  : 2;
		uint8_t ospeedr7  : 2;
		uint8_t ospeedr8  : 2;
		uint8_t ospeedr9  : 2;
		uint8_t ospeedr10 : 2;
		uint8_t ospeedr11 : 2;
		uint8_t ospeedr12 : 2;
		uint8_t ospeedr13 : 2;
		uint8_t ospeedr14 : 2;
		uint8_t ospeedr15 : 2;
	} bits;
	size_t val;
} gpio_ospeedr_t;

#define GPIO_SPEED_LOW	0b00
#define GPIO_SPEED_MED	0b01
#define GPIO_SPEED_HIGH 0b11

typedef union {
	struct {
		uint8_t pupdr0	: 2;
		uint8_t pupdr1	: 2;
		uint8_t pupdr2	: 2;
		uint8_t pupdr3	: 2;
		uint8_t pupdr4	: 2;
		uint8_t pupdr5	: 2;
		uint8_t pupdr6	: 2;
		uint8_t pupdr7	: 2;
		uint8_t pupdr8	: 2;
		uint8_t pupdr9	: 2;
		uint8_t pupdr10 : 2;
		uint8_t pupdr11 : 2;
		uint8_t pupdr12 : 2;
		uint8_t pupdr13 : 2;
		uint8_t pupdr14 : 2;
		uint8_t pupdr15 : 2;
	} bits;
	size_t val;
} gpio_pupdr_t;

#define GPIO_PULLUP_NO	 0b00
#define GPIO_PULLUP_UP	 0b01
#define GPIO_PULLUP_DOWN 0b10

typedef union {
	struct {
		bool idr0  : 1;
		bool idr1  : 1;
		bool idr2  : 1;
		bool idr3  : 1;
		bool idr4  : 1;
		bool idr5  : 1;
		bool idr6  : 1;
		bool idr7  : 1;
		bool idr8  : 1;
		bool idr9  : 1;
		bool idr10 : 1;
		bool idr11 : 1;
		bool idr12 : 1;
		bool idr13 : 1;
		bool idr14 : 1;
		bool idr15 : 1;
	} bits;
	size_t val;
} gpio_idr_t;

typedef union {
	struct {
		bool odr0  : 1;
		bool odr1  : 1;
		bool odr2  : 1;
		bool odr3  : 1;
		bool odr4  : 1;
		bool odr5  : 1;
		bool odr6  : 1;
		bool odr7  : 1;
		bool odr8  : 1;
		bool odr9  : 1;
		bool odr10 : 1;
		bool odr11 : 1;
		bool odr12 : 1;
		bool odr13 : 1;
		bool odr14 : 1;
		bool odr15 : 1;
	} bits;
	size_t val;
} gpio_odr_t;

typedef union {
	struct {
		bool bs0  : 1;
		bool bs1  : 1;
		bool bs2  : 1;
		bool bs3  : 1;
		bool bs4  : 1;
		bool bs5  : 1;
		bool bs6  : 1;
		bool bs7  : 1;
		bool bs8  : 1;
		bool bs9  : 1;
		bool bs10 : 1;
		bool bs11 : 1;
		bool bs12 : 1;
		bool bs13 : 1;
		bool bs14 : 1;
		bool bs15 : 1;
		bool br0  : 1;
		bool br1  : 1;
		bool br2  : 1;
		bool br3  : 1;
		bool br4  : 1;
		bool br5  : 1;
		bool br6  : 1;
		bool br7  : 1;
		bool br8  : 1;
		bool br9  : 1;
		bool br10 : 1;
		bool br11 : 1;
		bool br12 : 1;
		bool br13 : 1;
		bool br14 : 1;
		bool br15 : 1;
	} bits;
	struct {
		uint16_t br : 16;
		uint16_t bs : 16;
	} regs;
	size_t val;
} gpio_bsrr_t;

typedef union {
	struct {
		bool lck0  : 1;
		bool lck1  : 1;
		bool lck2  : 1;
		bool lck3  : 1;
		bool lck4  : 1;
		bool lck5  : 1;
		bool lck6  : 1;
		bool lck7  : 1;
		bool lck8  : 1;
		bool lck9  : 1;
		bool lck10 : 1;
		bool lck11 : 1;
		bool lck12 : 1;
		bool lck13 : 1;
		bool lck14 : 1;
		bool lck15 : 1;
		bool lckk  : 1;
	} bits;
	struct {
		uint16_t lck  : 16;
		bool	 lckk : 1;
	} regs;
	size_t val;
} gpio_lckr_t;

typedef union {
	struct {
		uint8_t afsel0 : 4;
		uint8_t afsel1 : 4;
		uint8_t afsel2 : 4;
		uint8_t afsel3 : 4;
		uint8_t afsel4 : 4;
		uint8_t afsel5 : 4;
		uint8_t afsel6 : 4;
		uint8_t afsel7 : 4;
	} bits;
	size_t val;
} gpio_afrl_t;

typedef union {
	struct {
		uint8_t afsel8	: 4;
		uint8_t afsel9	: 4;
		uint8_t afsel10 : 4;
		uint8_t afsel11 : 4;
		uint8_t afsel12 : 4;
		uint8_t afsel13 : 4;
		uint8_t afsel14 : 4;
		uint8_t afsel15 : 4;
	} bits;
	size_t val;
} gpio_afrh_t;

#define GPIO_ALTF_0 0b0000
#define GPIO_ALTF_1 0b0001
#define GPIO_ALTF_2 0b0010
#define GPIO_ALTF_3 0b0011
#define GPIO_ALTF_4 0b0100
#define GPIO_ALTF_5 0b0101
#define GPIO_ALTF_6 0b0110
#define GPIO_ALTF_7 0b0111

typedef union {
	struct {
		bool br0  : 1;
		bool br1  : 1;
		bool br2  : 1;
		bool br3  : 1;
		bool br4  : 1;
		bool br5  : 1;
		bool br6  : 1;
		bool br7  : 1;
		bool br8  : 1;
		bool br9  : 1;
		bool br10 : 1;
		bool br11 : 1;
		bool br12 : 1;
		bool br13 : 1;
		bool br14 : 1;
		bool br15 : 1;
	} bits;
	size_t val;
} gpio_brr_t;
