#pragma once

#if defined(stm32f101l)
#error Not implemented
#elif defined(stm32f101m)
#error Not implemented
#elif defined(stm32f101h)
#error Not implemented
#elif defined(stm32f102l)
#error Not implemented
#elif defined(set32f102m)
#error Not implemented
#elif defined(stm32f103l)
#error Not implemented
#elif defined(stm32f103m)
#include "../stm32f103m/systick.h"
#elif defined(set32f103h)
#include "../stm32f103h/systick.h"
#elif defined(stm32f105)
#error Not implemented
#elif defined(stm32f107)
#error Not implemented
#else
#error Board not selected
#endif

#include <stdbool.h>
#include <stddef.h>

/**
 * @brief Configure timer.
 *
 * @param[in] value Initial counter value.
 * @param[in] interrupt Enable interrupt.
 */
void systick_configure(size_t value, bool interrupt);

/**
 * @brief Start timer.
 */
void systick_start(void);

/**
 * @brief Stop timer.
 */
void systick_stop(void);

/**
 * @brief Get current timer value.
 * @return Current timer value.
 */
size_t systick_current(void);

/**
 * @brief Check timer expired.
 * @return Expired.
 */
bool systick_expired(void);
