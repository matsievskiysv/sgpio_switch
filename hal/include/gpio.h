#pragma once

#if defined(stm32f030f4)
#include "../stm32f030f4/gpio.h"
#else
#error Board not selected
#endif

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

/**
 * @brief Configure GPIO pin.
 *
 * @param[in] back GPIO bank.
 * @param[in] pin_number GPIO pin.
 * @param[in] mode Electrical mode.
 * Possible values are GPIO_MODE_INPUT, GPIO_MODE_ANALOG.
 * @param[in] pull Pull up/pull down mode.
 * Possible values are GPIO_PULLUP_NO, GPIO_PULLUP_UP, GPIO_PULLUP_DOWN.
 */
void gpio_configure_input(size_t bank, uint8_t pin_number, uint8_t mode, uint8_t pull);

/**
 * @brief Configure GPIO pin.
 *
 * @param[in] back GPIO bank.
 * @param[in] pin_number GPIO pin.
 * @param[in] mode Electrical mode.
 * Possible values are GPIO_OUT_PUSHPULL, GPIO_OUT_OPENDRAIN.
 * @param[in] pull Pull up/pull down mode.
 * Possible values are GPIO_PULLUP_NO, GPIO_PULLUP_UP, GPIO_PULLUP_DOWN.
 * @param[in] speed GPIO speed.
 * Possible values are GPIO_SPEED_LOW, GPIO_SPEED_MED and GPIO_SPEED_HIGH.
 */
void gpio_configure_output(size_t bank, uint8_t pin_number, uint8_t mode, uint8_t pull, uint8_t speed);

/**
 * @brief Configure GPIO pin.
 *
 * @param[in] back GPIO bank.
 * @param[in] pin_number GPIO pin.
 * @param[in] pull Pull up/pull down mode.
 * Possible values are GPIO_PULLUP_NO, GPIO_PULLUP_UP, GPIO_PULLUP_DOWN.
 */
void gpio_configure_alt(size_t bank, uint8_t pin_number, uint8_t pull);

/**
 * @brief Read GPIO pins.
 *
 * @param[in] back GPIO bank.
 * @param[in] pin_mask GPIO pin.
 * @return GPIO pin input value.
 */
size_t gpio_read_pins(size_t bank, size_t pin_mask);

/**
 * @brief Read GPIO pin.
 *
 * @param[in] back GPIO bank.
 * @param[in] pin_number GPIO pin.
 * @return GPIO pin input value.
 */
bool gpio_read_pin(size_t bank, uint8_t pin_number);

/**
 * @brief Write GPIO pins.
 *
 * @param[in] back GPIO bank.
 * @param[in] pin_mask GPIO pin mask.
 * @param[in] value GPIO pins value.
 */
void gpio_write_pins(size_t bank, size_t pin_mask, bool value);

/**
 * @brief Write GPIO pin.
 *
 * @param[in] back GPIO bank.
 * @param[in] pin_number GPIO pin mask.
 * @param[in] value GPIO pin value.
 */
void gpio_write_pin(size_t bank, uint8_t pin_number, bool value);
