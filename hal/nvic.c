#include <nvic.h>
#include <util.h>

void
enable_interrupt(size_t N)
{
	REG_T reg = GET_REG(NVIC_ISER, NVIC_REG_OFFSET(N));
	REG_SET(reg, NVIC_BIT(N));
}

void
disable_interrupt(size_t N)
{
	REG_T reg = GET_REG(NVIC_ICER, NVIC_REG_OFFSET(N));
	REG_SET(reg, NVIC_BIT(N));
}
