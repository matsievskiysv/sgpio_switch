#pragma once

#include <stddef.h>
#include <stdint.h>

#ifndef LED_PORT
#define LED_PORT GPIO_C
#endif // LED_PORT

#ifndef LED_PIN
#define LED_PIN 13
#endif // LED_PIN

#define FREQ		 8000000
#define BAUD		 115200
#define UART_PORT	 USART1
#define UART_GPIO_PORT	 GPIO_A
#define UART_PIN_TX	 9
#define UART_PIN_RX	 10
#define DATA_BUFFER_SIZE 60

uint8_t data_buffer[DATA_BUFFER_SIZE] = {0};

char prompt[] = "\n\r>";

enum state {
	LISTEN,
	ECHO,
	COPY,
	PROCESS,
	STATE_COUNT,
};

enum state current_state = LISTEN;
enum state next_state	 = LISTEN;

void change_state(enum state new);

void transmit_buffer(uint8_t *buffer, size_t count);

void send_string(char *string);

void echo_received(void);

void start_listen(void);

void process_input(void);

void run_callback(void);

char *commands[] = {"help", "hello", "bye", "flash size", "unique id", NULL};

void help_function(void);

void hello_function(void);

void bye_function(void);

void flash_size_function(void);

void chip_id_function(void);

void (*const functions[])(void) = {help_function, hello_function, bye_function, flash_size_function, chip_id_function};

void (*const state_transition[STATE_COUNT][STATE_COUNT])(void) = {
    // LISTEN
    {
	// LISTEN
	NULL,
	// ECHO
	echo_received,
	// COPY
	start_listen,
	// PROCESS
	NULL,
    },
    // ECHO
    {
	// LISTEN
	start_listen,
	// ECHO
	NULL,
	// COPY
	process_input,
	// PROCESS
	NULL,
    },
    // COPY
    {
	// LISTEN
	start_listen,
	// ECHO
	NULL,
	// COPY
	NULL,
	// PROCESS
	run_callback,
    },
    // PROCESS
    {
	// LISTEN
	start_listen,
	// ECHO
	NULL,
	// COPY
	NULL,
	// PROCESS
	NULL,
    },
};
